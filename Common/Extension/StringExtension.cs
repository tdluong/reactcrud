﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Extension
{
    public static class StringExtension
    {
        public static string EscapseSpecialCharacter(this string str)
        {
            return str.Replace("'", "\\'").Replace("*", "\\*").Replace(".", "\\.");
        }
    }
}
