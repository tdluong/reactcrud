﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Enum
{
    public enum ResponseStatus
    {
        /// <summary>
        /// not yet submit 
        /// </summary>
        NotSubmit = -1,

        /// <summary>
        /// Save failed
        /// </summary>
        ReturnFail = 0,

        /// <summary>
        /// Save successful
        /// </summary>
        ReturnSuccess = 1,

        /// <summary>
        /// Save successful
        /// </summary>
        ReturnWarning = 2,
    }
}
