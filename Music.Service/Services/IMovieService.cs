﻿using Music.Service.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Music.Service.Services
{
    public interface IMovieService
    {
        Task<SaveResponseDto> Create(MovieDto movieDto);
        Task<SaveResponseDto> Update(MovieDto movieDto);
        Task<SaveResponseDto> Delete(MovieDto movieDto);
        Task<SaveResponseDto> Delete(int id);
        Task<SaveResponseDto> Delete(int[] ids);
        Task<MovieDto> GetById(int id);
        Task<ModelPagingDto<MovieDto>> GetAll(string term, PageInfoDto pageInfo);
        Task<ModelPagingDto<MovieDto>> Get(PageInfoDto pageInfo);
        Task<ModelPagingDto<MovieDto>> GetAll();
        Task<ModelPagingDto<MovieDto>> Filter(PageInfoDto pageInfo, FilterDto[] filter);
    }
}
