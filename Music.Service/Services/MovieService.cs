﻿using AutoMapper;
using log4net;
using Music.Service.Dto;
using Music.Service.Models;
using Music.Service.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Music.Service.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepo;
        private readonly IMapper _mapper;
        private readonly ILog _logService;
        public MovieService(IMovieRepository movieRepo, IMapper mapper)
        {
            _movieRepo = movieRepo;
            _mapper = mapper;
            _logService = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
        public Task<SaveResponseDto> Create(MovieDto movieDto)
        {
            var movieEntity = _mapper.Map<Movie>(movieDto);
            var isSuccess = _movieRepo.Create(movieEntity).Result;
            return Task.FromResult(new SaveResponseDto { IsSuccess = isSuccess, Message = isSuccess ? "The record has been created" : "This title movie is exist", Id = movieDto.Id });
        }

        public Task<SaveResponseDto> Delete(MovieDto movieDto)
        {
            var movieEntity = _mapper.Map<Movie>(movieDto);
            var isSuccess = _movieRepo.Delete(movieEntity).Result;
            return Task.FromResult(new SaveResponseDto { IsSuccess = isSuccess, Message = "The record has been deleted", Id = movieDto.Id });
        }

        public Task<SaveResponseDto> Delete(int id)
        {
            var isSuccess = _movieRepo.Delete(id).Result;
            return Task.FromResult(new SaveResponseDto { IsSuccess = isSuccess, Message = "The record has been deleted", Id = id });
        }

        public Task<SaveResponseDto> Delete(int[] ids)
        {
            try
            {
                var isSuccess = _movieRepo.Delete(ids).Result;
                return Task.FromResult(new SaveResponseDto { IsSuccess = isSuccess, Message = "Delete successful" });
            }
            catch (Exception ex)
            {
                _logService.ErrorFormat("Failed to delete movie. Error message: {0}.", ex.Message);
                return Task.FromResult(new SaveResponseDto { IsSuccess = false, Message = ex.Message });
            }

        }

        public Task<ModelPagingDto<MovieDto>> GetAll(string term, PageInfoDto pageInfo)
        {
            //_logService.Debug("This is a test for get all at Service layer");
            long recordCount = 0;
            var movieEntities = _movieRepo.GetAll(term, pageInfo.sortBy, pageInfo.sortAsc, pageInfo.PageIndex, pageInfo.PageSize, out recordCount).Result;
            var modelPagingDto = new ModelPagingDto<MovieDto>()
            {
                data = _mapper.Map<List<MovieDto>>(movieEntities),
                count = recordCount
            };
            return Task.FromResult(modelPagingDto);
        }

        public Task<ModelPagingDto<MovieDto>> Filter(PageInfoDto pageInfo, FilterDto[] filter)
        {
            //_logService.Debug("This is a test for get all at Service layer");
            long recordCount = 0;
            var movieEntities = _movieRepo.Filter(pageInfo, filter, out recordCount).Result;
            var modelPagingDto = new ModelPagingDto<MovieDto>()
            {
                data = _mapper.Map<List<MovieDto>>(movieEntities),
                count = recordCount
            };
            return Task.FromResult(modelPagingDto);
        }

        public Task<ModelPagingDto<MovieDto>> Get(PageInfoDto pageInfo)
        {
            long recordCount = 0;
            var movieEntities = _movieRepo.Get(pageInfo.PageIndex, pageInfo.PageSize, out recordCount).Result;
            var modelPagingDto = new ModelPagingDto<MovieDto>()
            {
                data = _mapper.Map<List<MovieDto>>(movieEntities),
                count = recordCount
            };
            return Task.FromResult(modelPagingDto);
        }

        public Task<ModelPagingDto<MovieDto>> GetAll()
        {
            long recordCount = 0;
            var movieEntities = _movieRepo.GetAll().Result;
            var modelPagingDto = new ModelPagingDto<MovieDto>()
            {
                data = _mapper.Map<List<MovieDto>>(movieEntities),
                count = recordCount
            };
            return Task.FromResult(modelPagingDto);
        }

        public Task<MovieDto> GetById(int id)
        {
            var movieEntity = _movieRepo.GetById(id)?.Result;
            if (movieEntity != null)
                return Task.FromResult(_mapper.Map<MovieDto>(movieEntity));
            return null;
        }

        public Task<SaveResponseDto> Update(MovieDto movieDto)
        {
            var movieEntity = _mapper.Map<Movie>(movieDto);
            var isSuccess = _movieRepo.Update(movieEntity).Result;
            return Task.FromResult(new SaveResponseDto { IsSuccess = isSuccess, Message = isSuccess ? "The record has been updated" : "This title movie is not exist", Id = movieDto.Id });

        }
    }
}
