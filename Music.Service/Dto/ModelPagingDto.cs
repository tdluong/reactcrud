﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Music.Service.Dto
{
    public class ModelPagingDto<T>
    {
        public ModelPagingDto()
        {
        }
        public ModelPagingDto(IEnumerable<T> items, int page = 1, int offset = 10)
        {
            data = items.Skip((page - 1) * offset).Take(offset);
            count = items.Count();
        }

        public IEnumerable<T> data { get; set; }

        public long count { get; set; }
    }
}
