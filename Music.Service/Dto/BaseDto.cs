﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Music.Service.Dto
{
    public class BaseDto
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
    }
}
