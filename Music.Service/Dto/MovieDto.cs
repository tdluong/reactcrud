﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Music.Service.Dto
{
    public class MovieDto : BaseDto
    {
        [Required]
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [Required]
        [JsonProperty(PropertyName = "released")]
        public string Released { get; set; }

        [Required]
        [JsonProperty(PropertyName = "tagline")]
        public string Tagline { get; set; }
    }
}
