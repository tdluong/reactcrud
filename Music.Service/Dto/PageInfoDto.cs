﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Music.Service.Dto
{
    public class PageInfoDto
    {
        public PageInfoDto()
        {
            PageIndex = 1;
            PageSize = 10;
        }

        public PageInfoDto(int pageIndex)
        {
            PageIndex = pageIndex;
            PageSize = 10;
        }

        public PageInfoDto(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }

        public string sortBy { get; set; }
        public bool sortAsc { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }

    public class FilterDto
    {
        public string id { get; set; }
        public string value { get; set; }

    }

    public class FilterTest
    {
        public string sortBy { get; set; }
        public bool sortAsc { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public FilterDto filters { get; set; }
}
}
