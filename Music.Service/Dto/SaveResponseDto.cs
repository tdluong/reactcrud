﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Music.Service.Dto
{
    public class SaveResponseDto
    {
        public SaveResponseDto()
        {
            ResponseStatus = ResponseStatus.NotSubmit;
        }
        public int Id { get; set; }
        public string Message { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public bool IsSuccess { get; set; }
    }
}
