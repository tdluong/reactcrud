﻿using Music.Service.Dto;
using Music.Service.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Music.Service.Repository
{
    public interface IMovieRepository
    {
        Task<bool> Create(Movie model);
        Task<bool> Update(Movie model);
        Task<bool> Delete(Movie model);
        Task<bool> Delete(int id);
        Task<bool> Delete(int[] ids);
        Task<Movie> GetById(int id);
        Task<List<Movie>> Get(int pageIndex, int pageSize, out long recordCount);
        Task<List<Movie>> GetAll();
        Task<List<Movie>> GetAll(string term, string sortBy, bool sortAsc, int pageIndex, int pageSize, out long recordCount);
        Task<List<Movie>> Filter(PageInfoDto pageInfo, FilterDto[] filter, out long recordCount);
    }
}
