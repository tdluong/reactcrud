﻿using AutoMapper;
using Common.Extension;
using log4net;
using Music.Service.Dto;
using Music.Service.Models;
using Neo4jClient;
using Neo4jClient.Cypher;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Music.Service.Repository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly IGraphClient _graphClient;
        private readonly IMapper _mapper;
        private readonly ILog _logService;
        public MovieRepository(IGraphClient graphClient, IMapper mapper)
        {
            _graphClient = graphClient;
            if (!_graphClient.IsConnected)
            {
                _graphClient.Connect();
            }
            _mapper = mapper;
            _logService = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        public Task<bool> Create(Movie model)
        {
            try
            {
                var queryMaxId = _graphClient.Cypher.Match($"(m:Movie)").Return(() => new { MaxId = Return.As<int>("max(m.id)") });
                int maxId = queryMaxId.Results.Single().MaxId;

                //----- validate check exist title -----------
                var movieList = _graphClient.Cypher.Match("(movie:Movie)")
                   .Where((Movie movie) => movie.Title == model.Title)
                   .Return(movie => movie.As<Movie>());

                if (movieList.Results.Any())
                {
                    _logService.ErrorFormat("Failed to create movie with info: {0}. Error: the movie is existed in the system", JsonConvert.SerializeObject(model));
                    return Task.FromResult(false);
                }

                var query = _graphClient.Cypher
                    .Merge($"(movie:Movie {{id: {maxId + 1}, title: '{model.Title}', tagline: '{model.Tagline}', released: '{model.Released}'}})");
                query.ExecuteWithoutResults();
                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                _logService.ErrorFormat("Failed to create movie with info: {0}. Inner text exception: {1}. Error message: {2}.", JsonConvert.SerializeObject(model), ex.InnerException?.Message, ex.Message);
                return Task.FromResult(false);
            }
        }

        public Task<bool> Delete(Movie model)
        {
            var query = _graphClient.Cypher.Match("(movie:Movie)")
                .Where((Movie movie) => movie.Id == model.Id)
                .Delete("movie");
            query.ExecuteWithoutResults();
            return Task.FromResult(true);
        }

        /// <summary>
        /// We assum that the title of Movie node is ID of the record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> Delete(int id)
        {
            var query = _graphClient.Cypher.Match($"(movie:Movie {{id: {id}}})")
               .DetachDelete("movie");
            query.ExecuteWithoutResults();
            return Task.FromResult(true);
        }

        /// <summary>
        /// We assum that the title of Movie node is ID of the record
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public Task<bool> Delete(int[] ids)
        {
            var query = _graphClient.Cypher.Match("(movie:Movie)")
               .Where($"movie.id IN {ids} ")
               .DetachDelete("movie")
               .WithParams(new { ids });
            query.ExecuteWithoutResults();

            return Task.FromResult(true);
        }


        public Task<List<Movie>> Get(int pageIndex, int pageSize, out long recordCount)
        {
            var countList = _graphClient.Cypher.Match("(movie:Movie)").Return(movie => movie.Count()).Results;
            recordCount = countList.FirstOrDefault();
            var query = _graphClient.Cypher.Match("(movie:Movie)")
                                    .Return(movie => movie.As<Movie>());
            if (pageIndex > 0 || pageSize > 0)
            {
                var skip = (pageIndex - 1) * pageSize;
                query = query.Skip(skip).Limit(pageSize);
            }
            var results = query.Results;
            return Task.FromResult(results.ToList());
        }

        public Task<List<Movie>> Filter(PageInfoDto pageInfo, FilterDto[] filter, out long recordCount)
        {
            //_logService.Debug("This is a test at get all at Repository layer");
            var skip = pageInfo.PageIndex * pageInfo.PageSize;
            var query = _graphClient.Cypher.Match("(movie:Movie)");

            if (filter != null && filter.Length > 0)
            {
                for (int i = 0; i < filter.Length; i++)
                {
                    if (i == 0)
                        query = query.Where("movie." + filter[i].id.EscapseSpecialCharacter() + "=~'(?i).*" + filter[i].value.EscapseSpecialCharacter() + ".*'");
                    else
                        query = query.AndWhere("movie." + filter[i].id.EscapseSpecialCharacter() + "=~'(?i).*" + filter[i].value.EscapseSpecialCharacter() + ".*'");

                }
            }
            var countList = query.Return(movie => movie.Count()).Results;
            recordCount = countList.FirstOrDefault();
            var results = query.Return<Movie>("movie");

            if (!string.IsNullOrEmpty(pageInfo.sortBy))
            {
                var propertyInfo = typeof(Movie).GetProperty(pageInfo.sortBy, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (propertyInfo != null)
                {
                    var jsonName = propertyInfo.CustomAttributes?.FirstOrDefault().NamedArguments.FirstOrDefault().TypedValue.Value?.ToString();
                    if (!pageInfo.sortAsc)
                    {
                        results = results.OrderBy("movie." + jsonName ?? propertyInfo.Name);
                    }
                    else
                    {
                        results = results.OrderByDescending("movie." + jsonName ?? propertyInfo.Name);
                    }
                }
            }
            results = results.Skip(skip).Limit(pageInfo.PageSize);
            return Task.FromResult(results.Results.ToList());
        }

        public Task<List<Movie>> GetAll()
        {
            var query = _graphClient.Cypher.Match("(movie:Movie)")
                                    .Return(movie => movie.As<Movie>());
            var results = query.Results;
            return Task.FromResult(results.ToList());
        }


        /// <summary>
        /// This method to search Movie node by Linq
        /// We need to find a solution to search by indexing - To work with large data
        /// </summary>
        /// <param name="term"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public Task<List<Movie>> GetAll(string term, string sortBy, bool sortAsc, int pageIndex, int pageSize, out long recordCount)
        {
            //_logService.Debug("This is a test at get all at Repository layer");
            var skip = (pageIndex - 1) * pageSize;
            var query = _graphClient.Cypher.Match("(movie:Movie)");

            if (!string.IsNullOrEmpty(term))
            {
                term = term.EscapseSpecialCharacter();
                query = query.Where("movie.title=~'(?i).*" + term + ".*'")
                                .OrWhere("movie.tagline=~'(?i).*" + term + ".*'");
            }
            var countList = query.Return(movie => movie.Count()).Results;
            recordCount = countList.FirstOrDefault();
            var results = query.Return<Movie>("movie");

            if (!string.IsNullOrEmpty(sortBy))
            {
                var propertyInfo = typeof(Movie).GetProperty(sortBy, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (propertyInfo != null)
                {
                    var jsonName = propertyInfo.CustomAttributes?.FirstOrDefault().NamedArguments.FirstOrDefault().TypedValue.Value?.ToString();
                    if (!sortAsc)
                    {
                        results = results.OrderBy("movie." + jsonName ?? propertyInfo.Name);
                    }
                    else
                    {
                        results = results.OrderByDescending("movie." + jsonName ?? propertyInfo.Name);
                    }
                }
            }
            results = results.Skip(skip).Limit(pageSize);
            return Task.FromResult(results.Results.ToList());
        }

        /// <summary>
        /// Currently we use title as ID of Movie node
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Movie> GetById(int id)
        {
            var result = _graphClient.Cypher.Match($"(movie:Movie {{id: {id}}})")
                .Return(movie => movie.As<Movie>())
                .Results.FirstOrDefault();
            return Task.FromResult(result);
        }

        public Task<bool> Update(Movie model)
        {
            try
            {
                IDictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("released", model.Released);
                parameters.Add("tagline", model.Tagline);

                var query = _graphClient.Cypher
                    .Match("(movie:Movie)")
                    .Where((Movie movie) => movie.Id == model.Id)
                    .Set("movie.released = {released}, movie.tagline = {tagline}")
                    .WithParams(parameters);
                query.ExecuteWithoutResults();
                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                _logService.ErrorFormat("Failed to update movie with info: {0}. Inner text exception: {1}. Error message: {2}.", JsonConvert.SerializeObject(model), ex.InnerException?.Message, ex.Message);
                return Task.FromResult(false);
            }
        }
    }
}
