﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Music.Service.Models
{
    public class BaseNode
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
    }
}
