﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Music.Service.Models
{
    public class Movie : BaseNode
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "released")]
        public string Released { get; set; }

        [JsonProperty(PropertyName = "tagline")]
        public string Tagline { get; set; }
    }
}
