﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactCrud.Models
{

    public class Neo4jSetting
    {
        public string BoltUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
