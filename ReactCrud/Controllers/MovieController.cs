﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Music.Service.Services;
using log4net;
using Music.Service.Dto;
using Newtonsoft.Json;

namespace ReactCrud.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    public class MovieController : BaseController
    {
        private readonly IMovieService _movieService;
        private readonly ILog _logService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
            _logService = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        [HttpGet]
        public async Task<IActionResult> Get(PageInfoDto pageInfo)
        {
            return Ok(await _movieService.Get(pageInfo));
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAll(PageInfoDto pageInfo)
        {
            return Ok(await _movieService.GetAll());
        }

        [HttpGet("search")]
        public async Task<IActionResult> Search(string term, PageInfoDto pageInfo)
        {
            return Ok(await _movieService.GetAll(term, pageInfo));
        }

        [HttpGet("filter")]
        public async Task<IActionResult> Search(PageInfoDto pageInfo, string filter)
        {
            try
            {
                var movieFilter = JsonConvert.DeserializeObject<FilterDto[]>(filter);
                return Ok(await _movieService.Filter(pageInfo, movieFilter));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("detail/{id}")]
        public async Task<IActionResult> Detail(int id)
        {
            return Ok(await _movieService.GetById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Insert(MovieDto movie)
        {
            if (!ModelState.IsValid)
            {
                var response = new SaveResponseDto { IsSuccess = false, Message = this.GetErrorMessage() };
                return BadRequest(response);
            }
            var saveResult = await _movieService.Create(movie);
            return Ok(saveResult);
        }

        [HttpPut]
        public async Task<IActionResult> Update(MovieDto movie)
        {
            if (!ModelState.IsValid)
            {
                var response = new SaveResponseDto { IsSuccess = false, Message = this.GetErrorMessage() };
                return BadRequest(response);
            }
            return Ok(await _movieService.Update(movie));
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _movieService.Delete(id);
            if (result.IsSuccess)
                return Ok(result.Message);
            return BadRequest(result.Message);
        }
    }
}