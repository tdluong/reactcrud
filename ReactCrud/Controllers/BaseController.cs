﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ReactCrud.Controllers
{
    public class BaseController : Controller
    {
        [NonAction]
        protected string GetErrorMessage()
        {
            var errorMessages = (from item in ModelState.Values
                                 from a in item.Errors
                                 select a.ErrorMessage).ToList();
            var message = string.Format("{0}", string.Join(";", errorMessages));

            return message;
        }
    }
}