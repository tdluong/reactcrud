﻿using Microsoft.Extensions.DependencyInjection;
using Music.Service.Repository;
using Music.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactCrud
{
    public static class DependencyInjection
    {
        public static void Config(IServiceCollection services)
        {
            //For custom services
            services.AddTransient<IMovieRepository, MovieRepository>();
            services.AddTransient<IMovieService, MovieService>();
        }
    }
}
