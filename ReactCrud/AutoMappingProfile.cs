﻿using AutoMapper;
using Music.Service.Dto;
using Music.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactCrud
{
    public class AutoMappingProfile : Profile
    {
        public AutoMappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<MovieDto, Movie>().ReverseMap();
        }
    }
}
