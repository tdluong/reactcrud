import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Movies } from './components/Movies';
import { AddMovie } from './components/AddMovie';

export const routes = <Layout>
    <Route exact path='/' component={Home} />
    <Route path='/movies' component={Movies} />
    <Route path='/movie/new' component={AddMovie} />
    <Route path='/movie/edit/:id' component={AddMovie} />
</Layout>;
