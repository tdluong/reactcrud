import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';

// Import react-confirm-alert
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css' 

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

interface MovieResponse {
    data: MovieData[];
    count: number;
}

interface MovieDataState {
    data: MovieData[];
    pages: any;
    loading: boolean;
}

export class MovieData {
    id: number = 0; 
    title: string = "";
    released: string = "";
    tagline: string = "";
}

export class Movies extends React.Component<RouteComponentProps<{}>, MovieDataState> {
    constructor() {
        super();
        this.state = { data: [], pages: null, loading: true };
        this.fetchData = this.fetchData.bind(this);
        this.handleDelete = this.handleDelete.bind(this); 
    }

    private deleteConfirmation = (id: number) => {
        const movie = this.state.data.find(x => x.id === id) as MovieData;
        confirmAlert({
          title: 'Confirm to delete',
          message: 'Do you want to delete "' + movie.title + '" movie?',
          buttons: [
            {
              label: 'Yes',
              onClick: () => this.handleDelete(id)
            },
            {
              label: 'No',
            }
          ]
        })
      };

    // Handle Delete a movie  
    private handleDelete(id: number) {  
        fetch(`api/Movie/?id=` + id, {  
            method: 'delete'  
        }).then(data => {  
            this.setState(  
                {  
                    data: this.state.data.filter((rec) => {  
                        return (rec.id != id);  
                    })  
                });  
        });  
    }

    // Get movies data
    fetchData(state, instance) {
        const sorted = state.sorted;
        const sortBy = sorted && sorted.length ? sorted[0].id : null;
        const sortAsc = sorted && sorted.length ? !sorted[0].desc : null;
        const filter = state.filtered && state.filtered.length ? JSON.stringify(state.filtered) : null;
        this.setState({ loading: true });
        fetch(`api/Movie/filter?pageSize=${state.pageSize}&pageIndex=${state.page}&sortAsc=${sortAsc}&sortBy=${sortBy}&filter=${filter}`)
            .then(response => response.json() as Promise<MovieResponse>)
            .then(res => {
                this.setState({
                    data: res.data,
                    pages: Math.ceil(res.count / state.pageSize),
                    loading: false
                });
            });
    }

    public render() {
        let contents = this.renderMovies();
        return <div>
            <h1>Movies</h1>
            <a href="/movie/new" type="button" className="btn btn-success" role="button" >Create New</a>
            <p></p>
            {contents}
        </div>;
    }

    // Returns the HTML table to the render() method.  
    private renderMovies() {
        const { data, pages, loading } = this.state;
        return <div>
            <ReactTable
                columns={[
                    {
                        columns: [
                            {
                                Header: "Title",
                                accessor: "title",
                            },
                            {
                                Header: "Released",
                                accessor: "released"
                            },
                            {
                                Header: "Tagline",
                                accessor: "tagline",
                            },
                            {
                                header: 'link',
                                accessor: "id",
                                filterable: false,
                                Cell: ({ row }) => (
                                    <div>
                                        <Link to={{ pathname: `/movie/edit/${row.id}` }}>Edit</Link>&nbsp;|&nbsp;  
                                        <a role="button" className="action" onClick={(id) => this.deleteConfirmation(row.id)}>Delete</a> 
                                    </div>
                                )
                            }
                        ]
                    }
                ]}
                manual // Forces table not to paginate or sort automatically, so we can handle it server-side
                data={data}
                pages={pages} // Display the total number of pages
                loading={loading} // Display the loading overlay when we need it
                onFetchData={this.fetchData} // Request new data when things change
                filterable
                defaultPageSize={10}
                className="-striped -highlight"
            />
        </div>;
    }
}
