import * as React from 'react';  
import { RouteComponentProps } from 'react-router';  
import { MovieData } from './Movies';  
// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";

interface AddMovieDataState {  
    title: string;  
    loading: boolean;   
    movieData: MovieData;
}  
export class AddMovie extends React.Component<RouteComponentProps<{}>, AddMovieDataState> {  
    constructor(props) {  
        super(props);  
        this.state = { title: "", loading: true, movieData: new MovieData};  
        var _id = this.props.match.params["id"];  
        // This will set state for Edit Movie  
        if (_id) {  
            fetch('api/Movie/detail/' + _id)  
                .then(response => response.json() as Promise<MovieData>)  
                .then(data => {  
                    this.setState({ title: "Edit Movie", loading: false, movieData: data });  
                });  
        }  
        else {  
            this.state = { title: "Create Movie", loading: false, movieData: new MovieData };  
        }  
        // This binding is necessary to make "this" work in the callback  
        this.handleSave = this.handleSave.bind(this);  
        this.handleCancel = this.handleCancel.bind(this); 
    }  
    public render() {  
        let contents = this.state.loading  
            ? <p><em>Loading...</em></p>  
            : this.renderCreateForm();  
        return <div>  
            <h1>{this.state.title}</h1>  
            <hr />  
            {contents}  
        </div>;  
    }
    
    // This will handle the submit form event.  
    private handleSave(event) {  
        event.preventDefault();  
        const data = new FormData(event.target);  
        // PUT request for Edit Movie.  
        if (this.state.movieData.title) {  
            fetch('api/Movie', {  
                method: 'PUT',  
                body: data,  
            }).then((response) => response.json())  
                .then((responseJson) => {  
                    this.props.history.push("/movies");  
                })  
        }  
        // POST request for Add Movie.  
        else {  
            fetch('api/Movie', {  
                method: 'POST',  
                body: data,  
            }).then((response) => response.json())  
                .then((responseJson) => {  
                    this.props.history.push("/movies");  
                })  
        }  
    }  
    // This will handle Cancel button click event.  
    private handleCancel(e) {  
        e.preventDefault();  
        this.props.history.push("/movies");  
    }
    // Returns the HTML Form to the render() method.  
    private renderCreateForm() {  
        return (  
            <form onSubmit={this.handleSave} >  
                <div className="form-group row" >  
                    <input type="hidden" name="Id" value={this.state.movieData.id} />  
                </div>  
                < div className="form-group row" >  
                    <label className=" control-label col-md-12" htmlFor="Title">Title</label>  
                    <div className="col-md-4">  
                        <input className="form-control" type="text" name="Title" defaultValue={this.state.movieData.title} required />  
                    </div>  
                </div >  
                < div className="form-group row" >  
                    <label className=" control-label col-md-12" htmlFor="Released">Released</label>  
                    <div className="col-md-4">  
                    <input className="form-control" type="text" name="Released" defaultValue={this.state.movieData.released} required />
                    </div>  
                </div >  
                < div className="form-group row" >  
                    <label className=" control-label col-md-12" htmlFor="Tagline">Tagline</label>  
                    <div className="col-md-4">  
                        <input className="form-control" type="text" name="Tagline" defaultValue={this.state.movieData.tagline} required />  
                    </div>  
                </div >  
                <div className="form-group">  
                    <button type="submit" className="btn btn-primary">Save</button> &nbsp;  
                    <button className="btn btn-default" onClick={this.handleCancel}>Cancel</button>  
                </div >  
            </form >  
        )  
    }  
}